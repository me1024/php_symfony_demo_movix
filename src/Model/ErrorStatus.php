<?php

namespace App\Model;

class ErrorStatus {

    public function __construct() {}

    private $errors = [];
    public function getErrors() {
        return $this->errors;
    }

    public function addObjectFieldError($object,$field,$message) {
        $type = "objectFieldError";
        $this->errors[] = [
            "type"    => $type,
            "object"  => $object,
            "field"   => $field,
            "message" => $message,
        ];
    }

    public function hasErrors() {
        return count($this->errors);
    }

    public function findObjectFieldErrors($object,$field) {
        $errors = [];
        foreach($this->errors as $index => $error) {
            if($error['type'] == 'objectFieldError' && $error['object'] == $object && $error['field'] == $field) {
                $errors[] = $error;
            }
        }
        return $errors;
    }
}