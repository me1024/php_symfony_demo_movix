<?php

namespace App\Service;

use App\Entity\Movie;
use App\Model\ErrorStatus;

class MovieService
{
    public function filterInputMovie(Movie $movie) {
        // todo
    }

    public function validateMovie(Movie $movie): ErrorStatus
    {
        $object = "movie";
        $errorStatus = new ErrorStatus();
        
        $simpleName = "[A-Za-z0-9 -]+";
        $simpleDate = "[\d-]+";

        if($movie->getTitle() === null || $movie->getTitle() === "") {
            $errorStatus->addObjectFieldError($object,"title","Title field cannot be empty");
        } elseif(!preg_match("/^${simpleName}$/si", $movie->getTitle())) {
            $errorStatus->addObjectFieldError($object,"title","Title field has wrong format");
        }
        
        if($movie->getYear() === null || $movie->getYear() === "") {
            $errorStatus->addObjectFieldError($object,"year","Year field cannot be empty");
        } elseif($movie->getYear() <= 1900) {
            $errorStatus->addObjectFieldError($object,"year","Year field is too old");
        }

        if($movie->getDirection() !== null && $movie->getDirection() !== ""
            && !preg_match("/^${simpleName}(,\s*${simpleName})*$/si", $movie->getDirection())
        ) {
            $errorStatus->addObjectFieldError($object,"direction","Direction field has wrong format");
        }

        if($movie->getScenario() !== null && $movie->getScenario() !== ""
            && !preg_match("/^${simpleName}(,\s*${simpleName})*$/si", $movie->getScenario())
        ) {
            $errorStatus->addObjectFieldError($object,"scenario","Scenario field has wrong format");
        }

        if($movie->getGenres() !== null && $movie->getGenres() !== ""
            && !preg_match("/^${simpleName}(,\s*${simpleName})*$/si", $movie->getGenres())
        ) {
            $errorStatus->addObjectFieldError($object,"genres","Genres field has wrong format");
        }

        if($movie->getReleases() !== null && $movie->getReleases() !== ""
            && !preg_match("/^${simpleName}:${simpleDate}(,\s*${simpleName}:${simpleDate})*$/si", $movie->getReleases())
        ) {
            $errorStatus->addObjectFieldError($object,"genres","Genres field has wrong format");
        }

        return $errorStatus;
    }
}