<?php

namespace App\Entity;

use App\Repository\MovieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MovieRepository::class)
 */
class Movie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $direction;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $scenario;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $genres;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $releases;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageUrl;

    /**
     * @ORM\OneToMany(targetEntity=MovieVote::class, mappedBy="movie", orphanRemoval=true)
     */
    private $movieVotes;

    public function __construct()
    {
        $this->movieVotes = new ArrayCollection();
    }

    private const DEFAULT_IMAGE_URL = "images/defaultPoster.jpg";

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getDirection(): ?string
    {
        return $this->direction;
    }

    public function setDirection(?string $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    public function getScenario(): ?string
    {
        return $this->scenario;
    }

    public function setScenario(?string $scenario): self
    {
        $this->scenario = $scenario;

        return $this;
    }

    public function getGenres(): ?string
    {
        return $this->genres;
    }

    public function setGenres(?string $genres): self
    {
        $this->genres = $genres;

        return $this;
    }

    public function getReleases(): ?string
    {
        return $this->releases;
    }

    public function setReleases(?string $releases): self
    {
        $this->releases = $releases;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return strlen($this->imageUrl) > 0 ? $this->imageUrl : self::DEFAULT_IMAGE_URL;
    }

    public function setImageUrl(string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getDirectionArray(): ?array {
        $result = explode(",",$this->direction);
        return $result;
    }

    public function getScenarioArray(): ?array {
        $result = explode(",",$this->scenario);
        return $result;
    }

    public function getGenresArray(): ?array {
        $result = explode(",",$this->genres);
        return $result;
    }

    public function getReleasesArray(): ?array {
        $result = [];
        $arr = explode(",",$this->releases);
        foreach($arr as $key => $row) {
            $rowArr = explode(":",$row);
            $result[$rowArr[0]] = $rowArr[1];
        }
        return $result;
    }

    /**
     * @return Collection|MovieVote[]
     */
    public function getMovieVotes(): Collection
    {
        return $this->movieVotes;
    }

    public function addMovieVote(MovieVote $movieVote): self
    {
        if (!$this->movieVotes->contains($movieVote)) {
            $this->movieVotes[] = $movieVote;
            $movieVote->setMovie($this);
        }

        return $this;
    }

    public function removeMovieVote(MovieVote $movieVote): self
    {
        if ($this->movieVotes->removeElement($movieVote)) {
            // set the owning side to null (unless already changed)
            if ($movieVote->getMovie() === $this) {
                $movieVote->setMovie(null);
            }
        }

        return $this;
    }
}
