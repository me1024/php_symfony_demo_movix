<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\Criteria;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

use App\Entity\Movie;
use App\Entity\MovieVote;
use App\Service\MovieService;
use App\Model\ErrorStatus;

class MovixController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('movix/index.html.twig', [
            'controller_name' => 'MovixController',
        ]);
    }

    /**
     * @Route("/getMovies/{page}", defaults={"page"=1}, name="getMovies")
     */
    public function getMovies(Request $request, EntityManagerInterface $em, int $page): Response {
        $page   = $page < 1 ? 1 : $page;
        $limit  = 5;
        $offset = ($page-1) * $limit;
        $count  = $em->getRepository(Movie::class)->createQueryBuilder('m')
            ->select('count(m.id)')->getQuery()->getSingleScalarResult();
        $pages  = ceil($count / $limit);
        $criteria = Criteria::create()
            ->setMaxResults($limit)
            ->setFirstResult($offset);
        $movies = $em->getRepository(Movie::class)
            ->matching($criteria)->toArray();

        return $this->render('movix/movies/getMovies.html.twig', [
            'controller_name' => 'MovixController',
            'movies' => $movies,
            'page' => $page,
            'pages' => $pages,
        ]);
    }

    /**
     * @Route("/addMovie", name="addMovie", defaults={"id"=null})
     * @Route("/editMovie/{id}", name="editMovie")
     */
    public function addMovie(Request $request, MovieService $service, EntityManagerInterface $em, ?int $id): Response {
        $movie = null;
        $errorStatus = new ErrorStatus();
        if($id !== null) $movie = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->find($id);
        if(!$movie) {
            $movie = new Movie();
        }

        if($request->getMethod() == 'POST') {
            $movie
                ->setTitle($request->get('movie_title'))
                ->setYear($request->get('movie_year'))
                ->setDirection($request->get('movie_direction'))
                ->setScenario($request->get('movie_scenario'))
                ->setGenres($request->get('movie_genres'))
                ->setReleases($request->get('movie_releases'))
                ->setImageUrl($request->get('movie_imageUrl'));
            ;

            $errorStatus = $service->validateMovie($movie);

            if(!$errorStatus->hasErrors()) {
                $em->persist($movie);
                $em->flush();
                return $this->redirectToRoute('getMovies', ['page' => 1]);
            }
        }

        return $this->render('movix/movies/addMovie.html.twig', [
            'controller_name' => 'MovixController',
            'movie' => $movie,
            'errorStatus' => $errorStatus,
        ]);
    }

    /**
     * @Route("/removeMovie/{id}", name="removeMovie")
     */
    public function removeMovie(int $id): Response {
        return $this->render('movix/movies/removeMovie.html.twig', [
            'controller_name' => 'MovixController',
        ]);
    }

    /**
     * @Route("/voteMovie/{id}/{point}", name="voteMovie")
     */
    public function voteMovie(int $id, int $point, EntityManagerInterface $em, Security $security) {
        $point = $point <= -1 ? -1 : ($point >= 1 ? 1 : 0); 
        $user = $security->getUser();
        if($id !== null) $movie = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->find($id);
        if($movie && $user) {
            $movieVote = $em->getRepository(MovieVote::class)
                ->findOneBy(['user'=>$user->getId(),'movie'=>$movie->getId()]);
            if(!$movieVote) {
                $movieVote = new MovieVote();
            }
            $movieVote->setUser($user);
            $movieVote->setMovie($movie);
            $movieVote->setPoint($point);
            $em->persist($movieVote);
            $em->flush();
        }
        return $this->redirectToRoute('getMovies', ['page' => 1]);
    }
}
