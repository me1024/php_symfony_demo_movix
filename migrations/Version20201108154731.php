<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201108154731 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movie_vote DROP FOREIGN KEY FK_A976A46910684CB');
        $this->addSql('ALTER TABLE movie_vote DROP FOREIGN KEY FK_A976A4699D86650F');
        $this->addSql('DROP INDEX IDX_A976A4699D86650F ON movie_vote');
        $this->addSql('DROP INDEX IDX_A976A46910684CB ON movie_vote');
        $this->addSql('ALTER TABLE movie_vote ADD user_id INT NOT NULL, ADD movie_id INT NOT NULL, DROP user_id_id, DROP movie_id_id');
        $this->addSql('ALTER TABLE movie_vote ADD CONSTRAINT FK_A976A469A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE movie_vote ADD CONSTRAINT FK_A976A4698F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('CREATE INDEX IDX_A976A469A76ED395 ON movie_vote (user_id)');
        $this->addSql('CREATE INDEX IDX_A976A4698F93B6FC ON movie_vote (movie_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movie_vote DROP FOREIGN KEY FK_A976A469A76ED395');
        $this->addSql('ALTER TABLE movie_vote DROP FOREIGN KEY FK_A976A4698F93B6FC');
        $this->addSql('DROP INDEX IDX_A976A469A76ED395 ON movie_vote');
        $this->addSql('DROP INDEX IDX_A976A4698F93B6FC ON movie_vote');
        $this->addSql('ALTER TABLE movie_vote ADD user_id_id INT NOT NULL, ADD movie_id_id INT NOT NULL, DROP user_id, DROP movie_id');
        $this->addSql('ALTER TABLE movie_vote ADD CONSTRAINT FK_A976A46910684CB FOREIGN KEY (movie_id_id) REFERENCES movie (id)');
        $this->addSql('ALTER TABLE movie_vote ADD CONSTRAINT FK_A976A4699D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_A976A4699D86650F ON movie_vote (user_id_id)');
        $this->addSql('CREATE INDEX IDX_A976A46910684CB ON movie_vote (movie_id_id)');
    }
}
